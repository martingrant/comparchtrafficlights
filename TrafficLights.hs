module TrafficLights where

import HDL.Hydra.Core.Lib
import HDL.Hydra.Circuits.Combinational

------------------------------------------------------------------------
-- Version 1 of the Traffic Lights circuit

circuitV1 reset =
   (st_green, st_loopG1, st_loopG2, st_amber, st_red, st_loopR1, 
    st_loopR2, st_loopR3, st_loopA1,
    green, amber, red)
  where
    st_green = dff  (or2 reset st_loopA1)
    st_loopG1 = dff (and2 reset' st_green)
    st_loopG2 = dff (and2 reset' st_loopG1)
    st_amber = dff  (and2 reset' st_loopG2)
    st_red = dff  (and2 reset' st_amber)
    st_loopR1 = dff (and2 reset' st_red)
    st_loopR2 = dff (and2 reset' st_loopR1)
    st_loopR3 = dff (and2 reset' st_loopR2)
    st_loopA1 = dff (and2 reset' st_loopR3)  
        
    green    = orw  [st_green, st_loopG1, st_loopG2]
    amber    = orw  [st_amber, st_loopA1]
    red    = orw  [st_red, st_loopR1, st_loopR2, st_loopR3]
    
    reset'   = inv  reset
    
------------------------------------------------------------------------
-- Version 2 of the Traffic Lights circuit

circuitV2 reset walkRequest =
   (st_normal, st_loop1, st_amber, st_red, st_loopR1, st_loopR2, st_returnToNormal,
    green, amber, red, wait, walk)
  where
    st_normal = dff (or3 reset st_returnToNormal st_loop1)
    st_loop1 = dff (and3 reset' st_normal (inv walkRequest))
    
    st_amber = dff (and2 reset' walkRequest)
    st_red = dff (and2 reset' st_amber)
    st_loopR1 = dff (and2 reset' st_red)
    st_loopR2 = dff (and2 reset' st_loopR1)
    st_returnToNormal = dff (and2 reset' st_loopR2)
  
    green    = orw  [st_normal, st_loop1]
    amber    = orw  [st_amber, st_returnToNormal]
    red      = orw  [st_red, st_loopR1, st_loopR2]
    wait = orw [st_normal, st_loop1, st_amber, st_returnToNormal]
    walk = orw [st_red, st_loopR1, st_loopR2]    
    
    reset'   = inv  reset