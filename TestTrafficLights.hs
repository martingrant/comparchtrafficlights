module Main where

import HDL.Hydra.Core.Lib
import TrafficLights

------------------------------------------------------------------------
-- Main function
main :: IO ()
main = do
  run_while1 testdataV1
  
  run_while2 testdataV2

------------------------------------------------------------------------
-- Simulation driver for version 1

run_while1 input = runAllInput input output
  where

-- Extract input signals from input data
    reset = getbit input 0

-- Connect the circuit to its input and output signals
    (st_green, st_loopG1, st_loopG2, st_amber, st_red, st_loopR1,
     st_loopR2, st_loopR3, st_loopA1,
     green, amber, red)
        = circuitV1 reset 

-- Display the inputs and outputs
    output =
      [string "reset=", bit reset,

       string "\n   State: ",
       string "\n     st_green=", bit st_green,
       string "\n     st_loopG1=", bit st_loopG1,
       string "\n     st_loopG2=", bit st_loopG2,
       string "\n     st_amber=", bit st_amber,
       string "\n     st_red=", bit st_red,
       string "\n     st_loopR1=", bit st_loopR1,
       string "\n     st_loopR2=", bit st_loopR2,
       string "\n     st_loopR3=", bit st_loopR3,
       string "\n     st_loopA1=", bit st_loopA1,

       string "\n   Control outputs: ",
       string "\n     green=", bit green,
       string "\n     amber=", bit amber,
       string "\n     red=", bit red,
       string "\n"]

------------------------------------------------------------------------
-- Simulation driver for version 2

run_while2 input = runAllInput input output
  where

-- Extract input signals from input data
    reset = getbit input 0
    walkRequest = getbit input 1

-- Connect the circuit to its input and output signals
    (st_normal, st_loop1, st_amber, st_red, st_loopR1, st_loopR2, st_returnToNormal,
     green, amber, red, wait, walk)
        = circuitV2 reset walkRequest 

-- Display the inputs and outputs
    output =
      [string "reset=", bit reset,
       string "\nwalkRequest=", bit walkRequest,
       
       string "\n\t States: ",
       string "\n\t\t st_normal=", bit st_normal,
       string "\n\t\t st_loop1=", bit st_loop1,
       string "\n\t\t st_amber=", bit st_amber,
       string "\n\t\t st_red=", bit st_red,
       string "\n\t\t st_loopR1=", bit st_loopR1,
       string "\n\t\t st_loopR2=", bit st_loopR2,
       string "\n\t\t st_returnToNormal=", bit st_returnToNormal,

       string "\n\t Outputs: ",
       string "\n\t\t green=", bit green,
       string "\n\t\t amber=", bit amber,
       string "\n\t\t red=", bit red,
       string "\n\t\t wait=", bit wait,
       string "\n\t\t walk=", bit walk,
       
       string "\n"]
       
------------------------------------------------------------------------
-- Test data for circuts

testdataV1 =
-- reset 
   [[1],
    [0],
    [0],
    [0],
    [0],
    [0],
    [0],
    [0],
    [0],
    [0],
    [0],
    [0],
    [0],
    [0]]
    
testdataV2 =
-- reset walkRequest
   [[1, 0],
    [0, 0],
    [0, 0],
    [0, 1],
    [0, 0],
    [0, 0],
    [0, 0],
    [0, 0],
    [0, 0],
    [0, 0],
    [0, 0],
    [0, 0],
    [0, 0],
    [0, 0],
    [0, 0],
    [0, 1],
    [0, 0],
    [0, 0],
    [0, 0],
    [0, 0],
    [0, 0],
    [0, 0]]